from settings.local import *


IS_UNITTEST = True

SEND_EMAILS = False

DATABASES["default"]["TEST"] = {
    "NAME": "eager_cannon_test",
}

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
